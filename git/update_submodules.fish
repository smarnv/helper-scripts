#!/usr/bin/env fish

set was_stashed (
  git stash push -um 'Save changes before updating Git submodules' \
     | string match -rq '^Saved'
)

git submodule update --remote
git add .
git commit -m 'Update git submodules'

echo 'Will pull the latest changes from the default remote.'
if read_confirm
  git pull    # origin master
  git status
end

if [ $was_stashed ]
  git stash pop
end
git status
