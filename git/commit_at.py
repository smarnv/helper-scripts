#!/usr/bin/env python3
"""
Create/amend a Git commit at specified time or date.
"""

import argparse
import datetime as dt
import subprocess
from typing import List, Optional


# Mapping for commit action shortcut to Git commit command suffix.
SET_COMMIT_DATE = '--date {date}'
GIT_COMMIT_ACTIONS = {
    '.': SET_COMMIT_DATE,
    '!': '--amend',
    '!!': '--amend --no-edit',
    '@': f'--amend {SET_COMMIT_DATE}',
    '@@': f'--amend --no-edit {SET_COMMIT_DATE}',
}


def parse_commit_date(date_str: str) -> str:
    """
    Parse commit date from string with precision up to 1 second.
    """
    date_str = (
        date_str
        if date_str not in ('', 'now')
        else dt.datetime.now().isoformat()
    )

    if 'T' in date_str:
        # Use explicitly given date and time.
        commit_datetime = dt.datetime.fromisoformat(date_str)
    else:
        # Infer date to be in last 24 hours.
        commit_datetime = dt.datetime.combine(
            dt.date.today(), dt.time.fromisoformat(date_str)
        )

        if commit_datetime > dt.datetime.now(tz=commit_datetime.tzinfo):
            commit_datetime -= dt.timedelta(days=1)

    if not commit_datetime.second:
        # Add missing seconds.
        commit_datetime = commit_datetime.replace(
            second=dt.datetime.now(tz=commit_datetime.tzinfo).second
        )

    return commit_datetime.replace(microsecond=0).isoformat()


def parse_cli_arguments(
    cli_arguments: Optional[List[str]] = None,
) -> argparse.Namespace:
    """
    Parse command-line arguments.
    """
    parser = argparse.ArgumentParser(
        description='Create/amend a Git commit at specified time.',
        epilog='Target commit time is rounded with a precision of 1 second.',
    )

    parser.add_argument(
        'date',
        nargs='?',
        const='now',
        default='now',
        type=parse_commit_date,
        help=(
            'commit date and time or only time (in last 24h) in ISO format '
            '[Default: now]'
        ),
    )

    parser.add_argument(
        'action',
        choices=GIT_COMMIT_ACTIONS.keys(),
        nargs='?',
        const='.',
        default='.',
        help=(
            'action: . (commit), ! (amend last), @ (amend last + set author '
            'date), !!/@@ (+ no edit) [Default: .]'
        ),
    )

    return parser.parse_args(cli_arguments)


if __name__ == '__main__':
    args = parse_cli_arguments()
    cmd = (
        f'GIT_COMMITTER_DATE={args.date} git commit -v '
        + GIT_COMMIT_ACTIONS[args.action].format(date=args.date)
    )

    subprocess.run(cmd, shell=True, check=True)
