#!/usr/bin/env fish

function is_running -a container operating_system
  toolbox run -c $container awk -F'=' '/^NAME=/ { print $2 }' /etc/os-release \
     | grep -Eqi $operating_system
end

set existing_toolbox_containers (
  toolbox list | awk 'p {print $2}; EOF{p=0} /CONTAINER NAME/{p=1}' | grep -v 'distrobox'
)

echo "Will update installed software packages on: $existing_toolbox_containers"
read_confirm
or exit 1

for container in $existing_toolbox_containers
  echo ---\n"Updating software on toolbox container: $container"\n

  if is_running $container 'fedora'
    set update_cmd sudo dnf -y upgrade
  else if is_running $container 'debian|ubuntu'
    set update_cmd su -c 'apt update; apt -y full-upgrade'
  end

  toolbox run -c $container $update_cmd
end
