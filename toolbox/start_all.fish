#!/usr/bin/env fish

set existing_toolbox_containers (
  toolbox list | awk 'p {print $2}; EOF{p=0} /CONTAINER NAME/{p=1}'
)
set env_file /run/.containerenv

if [ -f $env_file ]
  set podman_cmd flatpak-spawn --host podman
else
  set podman_cmd podman
end

for container in $existing_toolbox_containers
  echo "Started toolbox container `$container`..."
  $podman_cmd start $container &> /dev/null
end

toolbox list
