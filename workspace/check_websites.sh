#!/bin/sh
#
# Show HTTP status codes and canonical URLs of websites.
#
# Follow HTTP redirects and avoid using HEAD requests as some servers might not
# support them well. Issue GET requests and completely ignore the response body
# since it is not relevant for checking whether the target URL is reachable.
#
# The config file should use a Markdown-like format. The following are ignored:
#  - empty lines and leading whitespace
#  - hyphen (-) or star (*), surrounded by intervals, in the beginning of lines
#  - lines starting with `#`, `~~`, or `<!--` – for Markdown strike-through,
#    shell-style comments, or HTML-style `<!-- one-line comments -->`.

default_filename='./websites.md'

if [ "$1" != 'all' -a "$1" != 'broken' ]; then
  echo 'Usage: check_websites (all|broken) [file]'
  echo
  echo 'Show HTTP status codes and canonical URLs of websites.'
  echo "If \`file\` is not given, they are read from \`$default_filename\`."
  exit
fi

filename=$([ -n "$2" ] && echo "$2" || echo "$default_filename")
urls=$(sed 's/^ *[-*] *//' $filename | egrep -v '^(#|<!--|~~|\s*$)')
fetch='curl -o /dev/null -sLw'

for url in $urls; do
  $fetch "%{http_code} $url → %{url_effective} (t=%{time_total}s)\n" "$url"
done | ([ "$1" = 'all' ] && cat || egrep -v '^(200|401)')
