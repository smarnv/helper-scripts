#!/bin/sh

# W293 blank line contains whitespace
# E701 multiple statements on one line (colon)
# E702 multiple statements on one line (semicolon)
# E126 continuation line over-indented for hanging indent
pep8 --ignore=W293,E701,E702,E126 $1
