#!/usr/bin/env fish

set env_file /run/.containerenv

if [ -f $env_file ]
  awk -F '=' '/^name=/ { print $2 }' $env_file | tr -d \"
else
  echo 'host system'
end
