#!/usr/bin/env fish
#
# Report time logged with `stl` for all tasks during given month.
#
# Requires: `stl_report`, [stl](https://pypi.org/project/stltimelogger/)

set no_args (count $argv)
if [ $no_args -lt 1 -o $no_args -gt 3 ]
  echo 'Usage: stl_report_all month [year]'
  echo
  echo 'Report time logged with `stl` for all tasks during given month.'
  exit
end

for file_path in "$HOME/.config/stl/tasks" "$HOME/.stl/tasks"
  if [ -f $file_path ]
    set stl_tasks_file $file_path
    break
  end
end

if [ -z $stl_tasks_file ]
  echo 'Currently only supported default `stl` tasks file locations.'
  exit 1
end

for task in (sed -r 's/^(.+)\t.*/\1/' $stl_tasks_file);
  if ! stl_report $task $argv
    exit 1
  end
  echo
end
