#!/usr/bin/env fish
#
# Update (refresh + overwrite) file list of logged tasks with `stl`.
#
# Requires: [stl](https://pypi.org/project/stltimelogger/)

for file_path in $HOME/.config/stl $HOME/.stl
  if [ -d $file_path ]
    set stl_config_dir $file_path
    break
  end
end

if [ -z $stl_config_dir ]
  echo 'Currently only supported default `stl` config dir locations.'
  exit 1
end

set stl_month_report_files (
  find $file_path -mindepth 2 -maxdepth 2 -type f \
     | string replace $file_path/ '' \
     | sort -u
)
set years_with_reports (
  string replace -fr '/\d+$' '' $stl_month_report_files | sort -u
)

echo 'Months with reported activity:'
for year in $years_with_reports
  echo "- $year:" (string replace -fr "^$year/" '' $stl_month_report_files)
end

read_confirm
or exit 1

set line_regex '^.*\t(.*)-.* .*\t(.*)$'
set month_task_pairs (
  string replace -r -- $line_regex '$1 $2' (
    cat $stl_config_dir/$stl_month_report_files
  ) | sort -u
)
set tasks (string replace -r -- '^.* ' '' $month_task_pairs | sort -u)

set task_file_contents (
  for task in $tasks
    set months (string replace -f -- " $task" '' $month_task_pairs)
    echo $task\t"$months"
  end | string replace -a -- ' ' ','
)

function print_new_task_file_contents
  echo $task_file_contents | string replace -a -- ' ' \n
end

set stl_tasks_file $stl_config_dir/tasks

echo \n'Pending changes to apply:'
git diff --no-index $stl_tasks_file (print_new_task_file_contents | psub)
echo 'Continuing will overwrite your `stl` tasks file.'

read_confirm
or exit 1

echo 'Overwriting yout `stl` tasks file...'
print_new_task_file_contents > $stl_tasks_file
echo 'Your `stl` tasks file was successfully updated.'
