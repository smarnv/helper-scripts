#!/usr/bin/env fish
#
# Report time logged with `stl` for a given task and month.
#
# Requires: [stl](https://pypi.org/project/stltimelogger/)

set no_args (count $argv)
if [ $no_args -lt 1 -o $no_args -gt 3 ]
  echo 'Usage: stl_report task [month [year]]'
  echo
  echo 'Report time logged with `stl` for a given task and month.'
  echo 'Defaults to current month if only task name is specified.'
  exit
end

set months (string split ' ' 'jan feb mar apr may jun jul aug sep oct nov dec')
set today (string split '-' (date -I))
set year $today[1]
set month $today[2]

set task $argv[1]
if [ $no_args -ge 2 ]; set month $argv[2]; end
if [ $no_args -ge 3 ]; set year $argv[3]; end

set month (string lower -- (string trim -lc '0' -- $month))
set month_numbers (seq (count $months))

if ! contains -- $month $month_numbers
  for index in $month_numbers
    if [ $month = $months[$index] ]
      set match_index $index
      break
    end
  end

  if [ -z $match_index ]
    echo 'Please enter a valid month:'
    echo ' - either an integer between 1 and 12,'
    echo ' - or one of:' (string join ', ' $months).
    exit 1
  end

  set month $match_index
end

set replace_with_duration string replace -r ".*$task \((.*?)\).*" '$1'
set total_duration (stl show -m $month $year | $replace_with_duration --filter)

set durations (
  for date in $year-$month-(seq (date -d "$year-$month-1 +1 month -1 day" +%d))
    string join -- ': ' (
      stl show -d $date | grep -B 1 -- "$task" | $replace_with_duration)
  end | string replace -r -- '\[(.*)\]:' ' - $1:'
)

if test -z (string join '\n' $durations)
  echo "Didn't work on '$task' during $months[$month] $year."
else
  echo "Worked on '$task' during $months[$month] $year: $total_duration"
  echo -e (string join '\n' $durations)
end
