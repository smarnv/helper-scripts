#!/usr/bin/env python3

from email.mime.text import MIMEText
from smtplib import SMTP

msg = MIMEText('''\
Saluton,

Mi estas unu Pitonskripto kaj mi nun sukcese uzas retpoŝton! :)

Ĉion bonan,
Pitono Pitonisto
''')

msg['Subject'] = 'Saluton, Mondo!'
msg['From'] = 'Pitonskripto <sendanto@domajno.testo>'
msg['To'] = 'ricevanto@domajno.testo'

s = SMTP('localhost')
try:
    s.send_message(msg)
    print('retmesaĝo sendita!')
except:
    print('retmesaĝo malsendita!')
s.quit()
