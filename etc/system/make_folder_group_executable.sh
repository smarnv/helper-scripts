#!/bin/sh

# Gives *only* your group members **executable** access to current folder, its
# files and all of its subfolders, together with their files, recursively.
chmod 770 . -R
