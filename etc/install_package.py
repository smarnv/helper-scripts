#!/usr/bin/env python3
"""
Install the packages with names like the program arguments.
"""

import itertools as it
import subprocess
import sys
from typing import Iterator


def get_install_commands(packages: str) -> Iterator[str]:
    """
    Return all supported install commands.
    """
    system_upgrade_commands = {
        'fedora': ['dnf -y upgrade', 'rpm-ostree upgrade'],
        'debian': ['apt -y full-upgrade'],
        'arch': ['pacman --noconfirm -Syu'],
    }

    package_install_commands = {
        'fedora': [
            f'dnf -y install {packages}',
            f'rpm-ostree install --idempotent {packages}',
        ],
        'debian': [f'apt -y install {packages}'],
        'arch': [f'pacman --noconfirm -S {packages}'],
    }

    assert (
        system_upgrade_commands.keys() == package_install_commands.keys()
    ), 'Distribution list mismatch between install and system upgrade commands'

    return (
        it.chain(*system_upgrade_commands.values())
        if packages == 'updates'
        else it.chain(*package_install_commands.values())
    )


def try_run(install_command: str):
    """
    Try to run the given install command string.
    """
    command_parts = install_command.split()
    package_manager = command_parts[0]

    result = subprocess.run(
        ['type', '-t', package_manager], stdout=subprocess.PIPE
    )

    if result.returncode:
        # Package manager is not found on system.
        return

    if package_manager == 'rpm-ostree':
        # Allows to run from within a toolbox container.
        result = subprocess.run(command_parts)
    elif package_manager == 'apt':
        # Debian might not have `sudo` installed.
        result = subprocess.run(['su', '-c', install_command])
    else:
        result = subprocess.run(['sudo'] + command_parts)

    # Do not process further attempts.
    exit(result.returncode)


if __name__ == '__main__':
    packages = ' '.join(sys.argv[1:])

    for command in get_install_commands(packages):
        try_run(command)
