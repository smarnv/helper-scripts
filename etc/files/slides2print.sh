#!/bin/sh

if [ -x "/usr/bin/pdfjam" ]; then
	pdfjam "$@" --a4paper --no-landscape --frame true --nup 1x2 --suffix 1x2 --delta "2cm 2cm" --scale 0.9 --outfile finished.pdf
else
	echo "Could not find a program to join files: $@"
fi
