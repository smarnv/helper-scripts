#!/usr/bin/env fish

for mode_perm_pair in 'user -4000' 'group -2000'
  echo $mode_perm_pair | read mode perms

  echo "Files with set-$mode-ID bit:"
  string repeat -n 42 '-'

  sudo find / -perm $perms -type f -exec ls -ld {} \; 2> /dev/null \
    # | sed '\,\.local/share/containers,d'
    | awk '$0!~v' v='.local/share/containers'
    # | string match -rv 'containers'

  echo
end
