#!/bin/sh
#
# dmanager.sh - quickly manage user desktop on multiple displays with `xrandr`

internal_pattern='LVDS'
external_pattern='VGA|DP'

function print_usage() {
  echo 'usage: dmanager (list|left|above|prefer|switch) [<args>]'
  echo
  echo 'Quickly manage user desktop on multiple displays with `xrandr`.'
  echo
  echo '   list         list available displays and their properties'
  echo '   left A B     use two displays, A extends B to the left'
  echo '   above A B    use two displays, A extends B to the top'
  echo '   prefer A B   use only A, switch off display B'
  echo '   prefer A     use only A, A should be in (internal|external)'
  echo '   switch       use other display, change between internal<->external'
  exit 1
}

function apply_configuration() {
  case $1 in
    'list')
      xrandr --query
      ;;
    'left')
      xrandr --output $2 --auto --left-of $3
      xrandr --output $3 --auto --right-of $2
      ;;
    'above')
      xrandr --output $2 --auto --above $3
      xrandr --output $3 --auto --below $2
      ;;
    'prefer')
      xrandr --output $3 --off
      xrandr --output $2 --auto --preferred
      ;;
    'switch')
      displays=$(xrandr --listactivemonitors)
      no_displays=$(echo "$displays" | sed 's/Monitors: //;1q;d')
      [ $no_displays -ne 1 ] && print_usage

      connected_to=$(echo "$displays" | grep "0:" | awk '{ print $NF }')
      if (echo $connected_to | egrep -iq $internal_pattern); then
        eval "$0 prefer external"; exit
      elif (echo $connected_to | egrep -iq $external_pattern); then
        eval "$0 prefer internal"; exit
      else
        echo "Error when trying to switch away from display $connected_to."
        exit 1
      fi
      ;;
    *)
      print_usage
  esac

  set_user_desktop="$HOME/.local/bin/xdefault_desktop"
  [ -x $set_user_desktop ] && { $set_user_desktop > /dev/null 2>&1 & }
  exit
}

[ "$1" = 'list' ] || [ "$1" = 'switch' ] && apply_configuration $1
[ -z "$1" ] || [ -z "$2" ] && print_usage

if [ "$2" = 'internal' ]; then
  pattern_a=$internal_pattern; pattern_b=$external_pattern
elif [ "$2" = 'external' ]; then
  pattern_a=$external_pattern; pattern_b=$internal_pattern
else
  pattern_a=$2; pattern_b=$3
fi

display_ids=$(xrandr --query | grep ' connected' | awk '{ print $1; }')
function get_matching_display() {
  # return the alphabetically first display ID that matches the given pattern
  echo "$display_ids" | egrep -i $1 | sort -bfi | head -n 1
}

display_a=$(get_matching_display $pattern_a)
display_b=$(get_matching_display $pattern_b)

if [ -z "$display_a" ] || [ -z "$display_b" ]; then
  echo "Invalid patterns: $pattern_a $pattern_b. Valid IDs are:" $display_ids
  exit 1
fi

apply_configuration $1 $display_a $display_b
