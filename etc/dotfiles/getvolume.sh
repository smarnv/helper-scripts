#!/bin/sh

# Please make sure that you are using `mawk` instead of `gawk`, or any other
# version of `awk` that is different from `mawk`. Otherwise, this script will
# probably *not* work properly!

# The fields of interest may have different positions, so you should change
# them to their appropriate values in order to have a working script.

echo `amixer get 'Master' | awk -F'[\[\]]' '{if ($4 == "off") {print "MUT"; } else { print $2; }}' | tail -n 1`

exit 0
