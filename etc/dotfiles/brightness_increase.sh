#!/bin/sh

STATUS="$(xbacklight -get)"
CHECK="${STATUS}"

if [ $CHECK == 0.000000 ]; then
	xbacklight -inc 1
	pkill -RTMIN+1 i3blocks
else
	xbacklight -inc 10
	pkill -RTMIN+1 i3blocks
fi

