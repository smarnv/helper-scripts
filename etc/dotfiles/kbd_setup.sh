#!/bin/sh

setxkbmap -layout 'intl,intl,intl' -variant 'lat,cyr,gr' -option 'grp:shifts_toggle'
[ -e ~/.Xmodmap ] && { xmodmap ~/.Xmodmap; }
xset b off
