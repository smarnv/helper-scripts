#!/bin/sh

# Please make sure that you are using `mawk` instead of `gawk`, or any other
# version of `awk` that is different from `mawk`. Otherwise, this script will
# probably *not* work properly!

# The fields of interest may have different positions, so you should change
# them to their appropriate values in order to have a working script.

caps=`xset -q | awk -F'Caps Lock:' '{print $2}' | egrep -m 1 . | awk '{print $1}'`
num=`xset -q | awk -F'Num Lock:' '{print $2}' | egrep -m 1 . | awk '{print $1}'`
scroll=`xset -q | awk -F'Scroll Lock:' '{print $2}' | egrep -m 1 . | awk '{print $1}'`

output=" "
[ $caps   = "on" ] && output=${output}"CAPS "
[ $num    = "on" ] && output=${output}"NUM "
[ $scroll = "on" ] && output=${output}"SCROLL "

echo "$output"
exit 0
