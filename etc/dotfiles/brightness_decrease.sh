#!/bin/sh

STATUS="$(xbacklight -get)"
CHECK="${STATUS}"

if [ $CHECK == 100.000000 ]; then
	xbacklight -dec 9
	pkill -RTMIN+1 i3blocks
else
	xbacklight -dec 10
	pkill -RTMIN+1 i3blocks
fi

