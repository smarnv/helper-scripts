#!/usr/bin/env fish
#
# Print list of layered packages in the current `rpm-ostree` deployment.

echo (rpm-ostree status -b --json 2> /dev/null \
   | jq '.deployments[0].packages[]' \
   | string trim -c \" -- \
   | sort -u)
