#!/usr/bin/env fish
#
# Usage: list_not_installed_packages [remote]
#
# Print list of available but not installed packages from a Flatpak remote.
# If `remote` is omitted, then all registered remotes are used implicitly.

set column_names Name Description 'Application ID' Version Origin
set max_widths 20 40 20 5 15
set no_columns (count $column_names)

# Some character not used anywhere in the interim outputs.
# Bad values: [a-zA-z0-9.,:!@"#'%^+-_()~`]
set sep '|'

# Return the larger of two numerical values.
function max -a n1 n2
  [ $n1 -ge $n2 ]; and echo $n1; or echo $n2
end

# Column names should not be trimmed.
set max_widths (
  for idx in (seq $no_columns)
    max $max_widths[$idx] (string length $column_names[$idx])
  end
)

# Print not installed apps from Flatpak remote in internal representation.
function list_not_installed_apps -a remote
  set installed_apps (flatpak list --app --columns=app)
  set exclude_pattern (
    string join '|' (string escape --style=regex $installed_apps))

  set column_ids (string match -r '^\w+' (string lower $column_names))
  set available_apps (
    flatpak remote-ls --app --columns=(string join ',' $column_ids) $remote)

  set raw_list (string match -vr $exclude_pattern $available_apps)

  # Display column names on first line.
  string join -- $sep $column_names
  string join -- $sep (for w in $max_widths; string repeat -n $w '-'; end)

  # Trim values with the maximum allowed text width for each column.
  for table_row in $raw_list
    set cells (string split \t $table_row)
    string join -- $sep (
      for idx in (seq $no_columns)
        string sub -l $max_widths[$idx] $cells[$idx]
      end)
  end | sort --ignore-case
end

# Print final list of apps available remotely but not installed locally.
list_not_installed_apps $argv \
   | column -ts $sep -o ' | ' \
   | less -R  # Use paging as output can become quite long.
