#!/usr/bin/env fish
#
# Ask user to confirm if they want to continue with some operation.

read -P 'Would you like to continue? [yes/NO] ' -n 3 -l choice
[ (string lower "$choice") = 'yes' ]; or exit 1
