#!/usr/bin/env fish
#
# Run command indefinitely with countdown of a number of seconds to sleep.

set timeout (math $argv[1])
set command $argv[2..]
set format_str '\r%'(math max 3, (string length $timeout) + 1)'d '

while true
  date
  eval $command

  for i in (seq $timeout -1 1)
    printf $format_str $i
    sleep 1s
  end
  clear
end
