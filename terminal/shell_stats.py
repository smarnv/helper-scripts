#!/usr/bin/env python3

import argparse
import math
import os
import shlex
import subprocess
from collections import Counter
from pathlib import PosixPath


DEFAULT_NUMBER_OF_LINES = 10

# Do not assume locations of history files. Use shell history commands instead.
SHELL_HISTORY_COMMANDS = {
    'fish': '-c history',
    'bash': '-ic "set -o history; history"',
}


class PositiveNaturalNumber(int):
    """
    Representation of a natural number greater than zero.
    """

    def __new__(cls, number_str: str | int | float, *args, **kwargs):
        """
        Create a new positive natural number from string.

        Args:
            number_str: String representation of the number to parse.

        Returns:
            The parsed natural number.

        Raises:
            ValueError: if the input is not a valid positive natural number.
        """
        # Enforce using string representation as input.
        number_str = str(number_str)

        # Accept exact decimals as valid representation of integers.
        if '.' in number_str:
            number_str = number_str.rstrip('0')
            if number_str.endswith('.'):
                number_str = number_str[:-1]

        number = super().__new__(cls, number_str, *args, **kwargs)

        if number <= 0:
            raise ValueError(f'Not a positive natural number: {number_str}')

        return number


class ShellPath(PosixPath):
    """
    Representation of a path to an interactive shell.
    """

    def __new__(cls, shell_path_str: str, *args, **kwargs):
        """
        Create a new path object pointing to a shell executable.

        Args:
            shell_path_str: String representation of the shell path.

        Returns:
            The parsed shell path.

        Raises:
            ValueError: if the input is not a valid shell path.
        """
        shell_path = super().__new__(cls, shell_path_str, *args, **kwargs)

        if shell_path.name not in SHELL_HISTORY_COMMANDS.keys():
            raise ValueError(f'Not a valid shell path: {shell_path_str}')

        return shell_path


def parse_args() -> argparse.Namespace:
    """
    Parse command-line arguments.

    Returns:
        The parsed command-line arguments.
    """
    parser = argparse.ArgumentParser(
        description='Show stats about most common shell commands.'
    )

    parser.add_argument(
        '-s',
        '--shell',
        type=ShellPath,
        default=ShellPath(os.environ.get('SHELL', 'bash')),
        help=(
            'path to shell to analyze (default: $SHELL, or `bash` if unset)'
            f'; one of: {tuple(SHELL_HISTORY_COMMANDS.keys())}'
        ),
    )

    _default_number_of_lines = PositiveNaturalNumber(DEFAULT_NUMBER_OF_LINES)
    parser.add_argument(
        '-n',
        '--lines',
        type=PositiveNaturalNumber,
        default=_default_number_of_lines,
        help=(
            'amount of most used commands to show'
            f' (default: {_default_number_of_lines})'
        ),
    )

    parser.add_argument(
        '-a',
        '--absolute',
        action='store_false',
        dest='use_relative_percentages',
        help='count percentages out of all commands in shell history',
    )

    return parser.parse_args()


def get_command_line_history(target_shell: ShellPath) -> list[str]:
    """
    Find history of previously executed shell commands.

    Args:
        target_shell: The shell to use.

    Returns:
        The current command-line history of target shell.
    """
    get_history_command = shlex.split(
        f'{target_shell} {SHELL_HISTORY_COMMANDS[target_shell.name]}'
    )

    result = subprocess.run(
        get_history_command, capture_output=True, check=True, text=True
    )

    lines = result.stdout.split('\n')

    if target_shell.name == 'bash':
        lines = [' '.join(line.strip().split()[1:]) for line in lines]

    return lines


def get_command_at(index: int, line_words: list[str]) -> str | None:
    """
    Get executed command from list of command parts.

    Args:
        index: Position at which to attempt to get the executed command.
        line_words: Words occurring at line.

    Returns:
        The parsed command.
    """
    try:
        command = line_words[index]

        if command == 'sudo':
            command = line_words[index + 1]
    except IndexError:
        return None

    if command.startswith('./'):
        command = 'local executable'

    if command in ('[', '[['):
        command = 'test'

    return command.strip('()/\\')


def get_executed_commands(*, command_line_history: list[str]) -> list[str]:
    """
    Find the executed commands in a command-line history.

    Args:
        command_line_history: Lines of shell history to analyze.

    Returns:
        The executed commands in history.
    """
    commands = []

    for line_words in map(str.split, command_line_history):
        if not line_words:
            continue

        index = 0
        while True:
            if command := get_command_at(index, line_words):
                commands.append(command)

            try:
                index = line_words.index('|', index) + 1
            except ValueError:
                break

    return commands


def print_top_commands(
    commands: list[str], *, count: int, use_relative_percentages: bool
) -> None:
    """
    Print the top commands in a list.

    Args:
        commands: Full list of commands to analyze.
        count: Number of commands to print.
        use_relative_percentages:
            Whether to find usage percentages only among filtered commands.
    """
    counter = Counter(commands)
    filtered_counter = {
        command: no_occurences
        for command, no_occurences in counter.most_common(count)
    }

    total_count = (
        sum(filtered_counter.values())
        if use_relative_percentages
        else counter.total()
    )
    output = [
        (command, no_occurences, 100 * no_occurences / total_count)
        for command, no_occurences in filtered_counter.items()
    ]

    index_width = int(math.log10(count)) + 1

    _, max_count = counter.most_common(1)[0]
    count_width = int(math.log10(max_count)) + 1

    formatted_output = [
        '   '.join(
            [
                '',
                str(index).rjust(index_width),
                str(no_occurences).rjust(count_width),
                f'{percentage:5.2f}%',
                command,
            ]
        )
        for index, (command, no_occurences, percentage) in enumerate(output, 1)
    ]

    for line in formatted_output:
        print(line)


if __name__ == '__main__':
    args = parse_args()
    commands = get_executed_commands(
        command_line_history=get_command_line_history(args.shell)
    )

    print_top_commands(
        commands,
        count=args.lines,
        use_relative_percentages=args.use_relative_percentages,
    )
